var mensagem = document.getElementById("campomensagem");
var button = document.querySelector(".btn");
var campochat = document.querySelector(".areachat");
button.addEventListener("click", () => {
    var mss = document.createElement("div");
    mss.className = "divgrande";
    var divcriada = document.createElement("div");
    divcriada.className = "divcriada";
    divcriada.innerHTML = `<p>${mensagem.value}</p>`
    var excluir = document.createElement("button");
    excluir.type = "button";
    excluir.innerHTML = "Excluir";
    excluir.className = "styleexcluir";
    var editar = document.createElement("button");
    editar.type = "button";
    editar.innerHTML = "Editar";
    editar.className = "styleeditar";
    campochat.appendChild(mss)
    mss.appendChild(divcriada)
    mss.appendChild(excluir)
    mss.appendChild(editar)
    campomensagem.value = "";

    excluir.addEventListener("click", () => {
        campochat.removeChild(mss)
    })

    editar.addEventListener("click", () => {
        var editdiv = document.createElement("textarea");
        var novobtn = document.createElement("button");
        novobtn.type = "button";
        novobtn.innerHTML = "Editar mensagem";
        editdiv.className = "editdiv";
        novobtn.className = "novobtn";
        divcriada.innerHTML = "";
        divcriada.append(editdiv)
        divcriada.append(novobtn)
        novobtn.addEventListener("click", () => {
            var novamensagem = document.getElementsByClassName("editdiv")
            // divcriada.removeChild(editdiv)
            // divcriada.removeChild(novobtn)
            divcriada.innerHTML = `<p>${novamensagem.value}</p>`            
        })
        
    })
})

